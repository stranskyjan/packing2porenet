import os
import re
from packing2porenet import export, ymport, Voronoi, PoreNetwork, Pore


def fileBase(key: str, iteration: int):
    return f"yade-{key}-{iteration:06d}"


def pores(i):
    print()
    print(f"pores() function start, i={i}")
    spheres = ymport.txt2spheres(fileBase("spheres", i) + ".txt")
    centers = [sphere.center for sphere in spheres]
    print("computing pores")
    voro = Voronoi(spheres)
    network = PoreNetwork().fromVoronoi(voro, warn=False, limitTetraQuality=0.8)
    (xmi, ymi, zmi), (xma, yma, zma) = [[f(center[index] for center in centers) for index in (0, 1, 2)] for f in (min, max)]

    def poreFilter(pore: Pore):
        sph = pore.sphere
        c = sph.center
        r = sph.radius
        x, y, z = c
        return x - r > xmi and x + r < xma and y - r > ymi and y + r < yma and z - r > zmi and z + r < zma

    network.filterPores(poreFilter)
    print("export 1")
    export.spheres2vtk(spheres, fileBase("packing", i))
    export.mesh2vtk(voro, fileBase("voro", i))
    print("merging pores")
    network.mergeIntersectingPores()
    print("export 2")
    pspheres = [pore.sphere for pore in network.cells]
    export.spheres2txt(pspheres, fileBase("pores", i))
    export.spheres2vtk(pspheres, fileBase("pores", i))
    export.poreNetwork2vtk(network, fileBase("throats", i))
    export.poreNetwork2txt(network, fileBase("network", i))
    print("pores() function end")
    print()


files = [f for f in os.listdir(".")]
exports = sorted(f for f in files if re.search("yade-spheres-\\d{6}\\.txt", f))
for e in exports:
    i = re.search("(?<=yade-spheres-)\\d{6}(?=\\.txt)", e)
    i = i.group()
    i = int(i)
    pores(i)

## Overview
Example of using packing2porenet with [YADE](https://yade-dem.org/)
Very simple compression test, default materials, default engines

## Usage
`YADE oedometric-compression.py`
where YADE can be yade, yadedaily, yade-some-extension etc.
`python3 pores.py`

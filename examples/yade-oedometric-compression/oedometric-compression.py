from yade import export

dimensions = (12, 12, 18)
# spheres

pred = pack.inAlignedBox((0, 0, 0), dimensions)
sp = pack.randomDensePack(pred, radius=1, rRelFuzz=0.2, returnSpherePack=True, spheresInCell=200)
sp.toSimulation()

# walls
walls = aabbWalls()
top = walls[5]
O.bodies.append(walls)

# apply oedometric compression
top.state.vel = (0, 0, -0.05)

# export spheres function every 5000 iterations
O.engines += [PyRunner(command="exportSpheres()", iterPeriod=5000, initRun=True)]


def exportSpheres():
    i = O.iter
    fName = f"yade-spheres-{i:06d}.txt"
    export.text(fName)


# 3D view
try:
    view = yade.qt.View()
    view.eyePosition = (-20, -20, 20)
    view.upVector = (0, 0, 1)
    view.viewDir = (1, 1, -.5)
except:
    pass

# run
O.stopAtIter = 15100
O.run()

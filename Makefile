######################################################################
# Makefile for packing2porenet project
# Run 'make help' for more info
######################################################################

######################################################################
# constants
######################################################################
PYTHON=python3

######################################################################
# targets
######################################################################
.PHONY: tests dist

help:
	@echo
	@echo "packing2porenet makefile"
	@echo
	@echo "targets:"
	@echo "  install"
	@echo "    runs 'python3 -m pip install' to install the application"
	@echo "  examples"
	@echo "    run examples"
	@echo "  test"
	@echo "    run unit tests"
	@echo "  clean"
	@echo "    cleans intermediate and auxiliary files"
	@echo "  dist"
	@echo "    creates distribution for PyPI by 'python3 -m build'"

install:
	$(PYTHON) -m pip install .

tests:
	make -C tests

clean:
	rm -rf build
	rm -rf dist
	rm -rf src/*.egg-info
	rm -rf src/*/__pycache__
	find . -type f -name '*.vtk' -delete
	find . -type f -name '*.pvsm' -delete
	find . -type f -name 'yade-*.txt' -delete
	find . -type f -name 'vtk-*.txt' -delete

dist:
	make clean
	$(PYTHON) -m build --no-isolation

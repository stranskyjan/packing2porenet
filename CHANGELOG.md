# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased] - yyyy-mm-dd

## [0.1.2] - 2022-04-29
### Added
- export.poreNetwork2txt
- throat radius
- export.poreNetwork2vtk
### Changed
- yade example restructuralized
### Renamed
- export.network2vtk -> export.mesh2vtk

## [0.1.1] - 2022-04-26
### Added
- using minieigen only optionally (numpy by default)

## [0.1.0] - 2022-04-25
### Initial release
- computes spherical pores based on spherical packing
- merging intersecting pores
